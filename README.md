# Skeleton Configuration Files

Skeleton configs for ArchLabs user home creation


Contents of the `home` folder should be copied to `/home/USER/` for existing users
or `/etc/skel/` for new users and/or system builders
